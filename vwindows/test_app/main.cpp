#include "vwindows.h"
#include "tray_icon.h"

int APIENTRY _tWinMain(
	_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPTSTR    lpCmdLine,
	_In_ int       nCmdShow) {

	vlib::vwindows::application app(hInstance, hPrevInstance);

	app.use(std::make_unique<tray_icon>());

	return app.run(lpCmdLine, nCmdShow);
}
