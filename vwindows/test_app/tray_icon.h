#pragma once
#include "vwindows.h"

class tray_icon : public vlib::vwindows::tray_icon {
public:

	void render() noexcept override;

	void click() noexcept override;
	void right_click() noexcept override;
	void context_menu() noexcept override;

	void show_context_menu();
};

