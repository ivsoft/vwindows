#pragma once

#include <string>
#include <list>
#include <Windows.h>

namespace vlib {
  namespace vwindows {
    class menu_item_builder;
    class application;

    class popup_menu {
    public:
      popup_menu() noexcept;
      virtual ~popup_menu() noexcept;

      virtual void render() noexcept = 0;

    protected:
      menu_item_builder& menu_item(const std::string& name)  noexcept;
      application& app() const  noexcept { return *_app; }

    private:
      friend class application;
      application* _app;
      HMENU _popupMenu;
      std::list<menu_item_builder> _items;

      void track(application* app, int x, int y, HWND notify_wnd) noexcept;

    };
  }
}