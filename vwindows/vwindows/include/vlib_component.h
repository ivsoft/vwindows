#pragma once
#include <Windows.h>

namespace vlib {
  namespace vwindows {
    class application;

    class component {
    public:
      virtual ~component();

      virtual bool initialize(application& app) noexcept = 0;
      virtual void refresh() noexcept = 0;
      virtual void destroy() noexcept = 0;

    protected:
      friend class application;
      virtual LRESULT notify(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) noexcept = 0;
    };
  }
}