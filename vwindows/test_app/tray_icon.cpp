#include "tray_icon.h"
#include "resource.h"
#include "popup_menu.h"

void tray_icon::render() noexcept {
	icon(IDI_SMALL);
	tip("AI-Tester writer");
}

void tray_icon::click() noexcept {
	show_context_menu();
}

void tray_icon::right_click() noexcept {
	show_context_menu();
}

void tray_icon::context_menu() noexcept {
	show_context_menu();
}

void tray_icon::show_context_menu() {
	app().track(std::make_unique<popup_menu>());
}
