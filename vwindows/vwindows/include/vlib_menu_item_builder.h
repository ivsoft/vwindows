#pragma once

#include <functional>
#include <Windows.h>

namespace vlib {
  namespace vwindows {
    class application;

    class menu_item_builder {
    public:
      menu_item_builder(application * app, HMENU hmenu, UINT item) noexcept;

      menu_item_builder& click(std::function<void()> handler) noexcept;

    private:
      application* _app;
      HMENU _hmenu;
      UINT _item;
    };
  }
}