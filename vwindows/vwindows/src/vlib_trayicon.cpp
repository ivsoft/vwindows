#include <Windows.h>
#include <tchar.h>
#include "vlib_trayicon.h"
#include "vlib_application.h"

vlib::vwindows::tray_icon::tray_icon() noexcept
  : _app(nullptr), _callback_message(0)
{
}

bool vlib::vwindows::tray_icon::initialize(application& app) noexcept
{
  _app = &app;

  _callback_message = _app->notify_wnd_add_handler(this);

  NOTIFYICONDATA nid;
  nid.cbSize = sizeof(nid);
  nid.hWnd = _app->notify_wnd();
  nid.uID = 1;
  nid.uFlags = NIF_MESSAGE;
  nid.uCallbackMessage = _callback_message;

  if (!Shell_NotifyIcon(NIM_ADD, &nid)) {
    _app->error("Create notify icon failed");
    return false;
  }

  nid.uVersion = NOTIFYICON_VERSION_4;
  Shell_NotifyIcon(NIM_SETVERSION, &nid);

  return true;
}

void vlib::vwindows::tray_icon::destroy() noexcept
{
  NOTIFYICONDATA nid;
  nid.cbSize = sizeof(nid);
  nid.hWnd = _app->notify_wnd();
  nid.uID = 1;

  Shell_NotifyIcon(NIM_DELETE, &nid);
}

vlib::vwindows::tray_icon::state_t::state_t() noexcept
  : icon(0)
{
}

void vlib::vwindows::tray_icon::icon(UINT iconId) noexcept
{
  _new_state.icon = iconId;
}

void vlib::vwindows::tray_icon::tip(const std::string& value) noexcept
{
  _new_state.tip = value;
}


void vlib::vwindows::tray_icon::click() noexcept
{
}
void vlib::vwindows::tray_icon::right_click() noexcept
{
}
void vlib::vwindows::tray_icon::context_menu() noexcept
{
}

void vlib::vwindows::tray_icon::refresh() noexcept
{
  render();

  NOTIFYICONDATA nid = {};
  nid.cbSize = sizeof(nid);
  nid.hWnd = _app->notify_wnd();
  nid.uID = 1;

  if (_current_state.icon != _new_state.icon) {
    _current_state.icon = _new_state.icon;

    nid.uFlags = NIF_ICON;
    nid.hIcon = LoadIcon(_app->hInst, MAKEINTRESOURCE(_new_state.icon));
    Shell_NotifyIcon(NIM_MODIFY, &nid);

  }

  if (_current_state.tip != _new_state.tip) {
    _current_state.tip = _new_state.tip;

    nid.uFlags = NIF_TIP | NIF_SHOWTIP;
    _tcscpy_s(nid.szTip, _current_state.tip.c_str());
    Shell_NotifyIcon(NIM_MODIFY, &nid);
  }
}

LRESULT vlib::vwindows::tray_icon::notify(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) noexcept
{
  if (_callback_message == message) {
    switch (LOWORD(lParam)) {
    case WM_LBUTTONUP:
      SetForegroundWindow(hWnd);
      click();
      PostMessage(hWnd, WM_NULL, 0, 0);
      return TRUE;
    case WM_RBUTTONUP:
      SetForegroundWindow(hWnd);
      right_click();
      PostMessage(hWnd, WM_NULL, 0, 0);
      return TRUE;
    case WM_CONTEXTMENU:
      SetForegroundWindow(hWnd);
      context_menu();
      PostMessage(hWnd, WM_NULL, 0, 0);
      return TRUE;
    }
  }

  return DefWindowProc(hWnd, message, wParam, lParam);
}