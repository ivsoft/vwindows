#pragma once

#include <string>
#include <Windows.h>
#include "vlib_component.h"

namespace vlib {
  namespace vwindows {
    class tray_icon : public component {
    public:
      tray_icon() noexcept;

      virtual void render() noexcept = 0;

      bool initialize(application& app) noexcept override;
      void refresh() noexcept override;
      void destroy() noexcept override;

    protected:
      application& app() const noexcept { return *_app; }
      void icon(UINT iconId) noexcept;
      void tip(const std::string &) noexcept;

      virtual void click() noexcept;
      virtual void right_click() noexcept;
      virtual void context_menu() noexcept;

      LRESULT notify(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) noexcept override;

    private:
      application* _app;

      struct state_t {
        UINT icon;
        std::string tip;

        state_t()  noexcept;
      };

      UINT _callback_message;
      state_t _current_state;
      state_t _new_state;

    };
  }
}