#include "vlib_menu_item_builder.h"
#include "vlib_application.h"

vlib::vwindows::menu_item_builder::menu_item_builder(application* app, HMENU hmenu, UINT item) noexcept
  : _app(app), _hmenu(hmenu), _item(item)
{
}

vlib::vwindows::menu_item_builder& vlib::vwindows::menu_item_builder::click(std::function<void()> handler) noexcept
{
  _app->set_cmd_notify_handler(_item, std::move(handler));

  MENUITEMINFO info;
  info.cbSize = sizeof(info);
  info.fMask = MIIM_STATE;
  info.fState = MFS_ENABLED;

  SetMenuItemInfo(_hmenu, _item, FALSE, &info);

  return *this;
}