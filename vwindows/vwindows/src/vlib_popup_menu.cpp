#include "vlib_popup_menu.h"
#include "vlib_application.h"
#include "vlib_menu_item_builder.h"

vlib::vwindows::popup_menu::popup_menu() noexcept
  : _app(nullptr), _popupMenu(nullptr)
{
}

vlib::vwindows::popup_menu::~popup_menu() noexcept
{
  if (nullptr != _popupMenu) {
    DestroyMenu(_popupMenu);
  }
}

void vlib::vwindows::popup_menu::track(application* app, int x, int y, HWND notify_wnd) noexcept
{
  _app = app;

  _popupMenu = CreatePopupMenu();

  render();

  TrackPopupMenu(_popupMenu, TPM_LEFTALIGN | TPM_LEFTBUTTON, x, y, 0, notify_wnd, NULL);

}

vlib::vwindows::menu_item_builder& vlib::vwindows::popup_menu::menu_item(const std::string& name)  noexcept
{
  MENUITEMINFO info;
  info.cbSize = sizeof(info);
  info.fMask = MIIM_ID | MIIM_STRING | MIIM_STATE;
  info.fType = MFT_STRING;
  info.fState = MFS_DISABLED;
  info.wID = _app->notify_next_cmd();
  info.dwTypeData = const_cast<char *>(name.c_str());

  InsertMenuItem(_popupMenu, (UINT)_items.size(), TRUE, &info);

  _items.emplace_back(_app, _popupMenu, info.wID);

  return *_items.rbegin();
}
