#pragma once

#include <list>
#include <string>
#include <map>
#include <memory>
#include <functional>
#include <Windows.h>
#include <tchar.h>

namespace vlib {
  namespace vwindows {
    class component;
    class tray_icon;
    class popup_menu;
    class menu_item_builder;

    class application {
    public:
      application(HINSTANCE hInstance, HINSTANCE hPrevInstance) noexcept;

      application& use(std::unique_ptr<component> control) noexcept;
      void exit(int exitCode) noexcept;

      int run(LPTSTR lpCmdLine, int nCmdShow) noexcept;

      void refresh() noexcept;

      void track(std::unique_ptr<popup_menu> menu) noexcept;

      //Logger
      void error(const std::string&) noexcept;


    private:
      friend class tray_icon;
      friend class popup_menu;
      friend class menu_item_builder;

      HINSTANCE hInst;
      std::list<std::unique_ptr<component>> _components;

      HWND _notify_wnd;
      std::map<UINT, component*> _notify_handlers;

      UINT _notify_next_cmd;
      std::map<UINT, std::function<void(void)>> _notify_cmd_handlers;

      HWND notify_wnd() noexcept;
      UINT notify_wnd_add_handler(component* control) noexcept;
      static LRESULT CALLBACK NotifyWndProcStatic(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) noexcept;
      LRESULT NotifyWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) noexcept;

      UINT notify_next_cmd() noexcept;
      void notify_reset_cmd(UINT saved_cmd) noexcept;
      void set_cmd_notify_handler(UINT cmd, std::function<void(void)> handler) noexcept;
    };
  }
}