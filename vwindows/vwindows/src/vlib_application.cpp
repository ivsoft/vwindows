#include "vlib_application.h"
#include "vlib_component.h"
#include "vlib_popup_menu.h"

vlib::vwindows::application::application(HINSTANCE hInstance, HINSTANCE hPrevInstance) noexcept
  :hInst(hInstance), _notify_wnd(nullptr), _notify_next_cmd(0)
{
}

vlib::vwindows::application& vlib::vwindows::application::use(std::unique_ptr<component> control) noexcept
{
  _components.emplace_back(std::move(control));
  return *this;
}
void vlib::vwindows::application::exit(int exitCode) noexcept
{
  PostQuitMessage(exitCode);
}
int vlib::vwindows::application::run(LPTSTR lpCmdLine, int nCmdShow) noexcept
{
  for (auto & component : _components) {
    if (!component->initialize(*this)) {
      return -1;
    }
  }

  for (auto & component : _components) {
    component->refresh();
  }

  MSG msg;

  while (GetMessage(&msg, nullptr, 0, 0)) {
    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }

  for (auto & component : _components) {
    component->destroy();
  }

  return (int)msg.wParam;
}

void vlib::vwindows::application::refresh() noexcept
{
  for (auto & component : _components) {
    component->refresh();
  }
}

UINT vlib::vwindows::application::notify_next_cmd() noexcept
{
  return ++_notify_next_cmd;
}

void vlib::vwindows::application::notify_reset_cmd(UINT saved_cmd) noexcept
{
  auto pos = _notify_cmd_handlers.lower_bound(saved_cmd);
  while (_notify_cmd_handlers.end() != pos) {
    pos = _notify_cmd_handlers.erase(pos);
  }

  _notify_next_cmd = saved_cmd;
}

void vlib::vwindows::application::track(std::unique_ptr<popup_menu> menu) noexcept
{
  POINT pt;
  GetCursorPos(&pt);

  auto save_cmd = _notify_next_cmd;

  menu->track(this, pt.x, pt.y, notify_wnd());

  auto destroy_cmd = notify_next_cmd();
  set_cmd_notify_handler(destroy_cmd, [this, save_cmd, m = menu.release()]() noexcept {
    delete m;
    notify_reset_cmd(save_cmd);
  });

  PostMessage(notify_wnd(), WM_COMMAND, MAKEWPARAM(destroy_cmd, 0), 0);
}

void vlib::vwindows::application::set_cmd_notify_handler(UINT cmd, std::function<void(void)> handler) noexcept
{
  _notify_cmd_handlers.emplace(cmd, std::move(handler));
}

LRESULT CALLBACK vlib::vwindows::application::NotifyWndProcStatic(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) noexcept
{
  auto pthis = reinterpret_cast<vlib::vwindows::application*>(GetWindowLongPtr(hWnd, GWLP_USERDATA));
  if (nullptr != pthis) {
    return pthis->NotifyWndProc(hWnd, message, wParam, lParam);
  }
  return DefWindowProc(hWnd, message, wParam, lParam);
}


HWND vlib::vwindows::application::notify_wnd() noexcept
{
  if (NULL == _notify_wnd) {
    WNDCLASSEX wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = &NotifyWndProcStatic;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInst;
    wcex.hIcon = NULL;
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = _T("VLIB Notify Class");
    wcex.hIconSm = NULL;

    RegisterClassEx(&wcex);

    _notify_wnd = CreateWindow(
      wcex.lpszClassName,
      _T("VLIB Notify Wnd"),
      WS_OVERLAPPED,
      CW_USEDEFAULT,
      0,
      CW_USEDEFAULT,
      0,
      nullptr,
      nullptr,
      hInst,
      nullptr);

    SetWindowLongPtr(_notify_wnd, GWLP_USERDATA, reinterpret_cast<INT_PTR>(this));
  }

  return _notify_wnd;
}

void vlib::vwindows::application::error(const std::string& msg) noexcept
{
  OutputDebugString(("Error: " + msg + "\n").c_str());
}

UINT vlib::vwindows::application::notify_wnd_add_handler(component* control) noexcept
{
  for (UINT message = WM_APP;; ++message) {
    if (_notify_handlers.end() == _notify_handlers.find(message)) {
      _notify_handlers.emplace(message, control);
      return message;         
    }
  }
}

LRESULT vlib::vwindows::application::NotifyWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) noexcept
{
  auto handler = _notify_handlers.find(message);
  if(_notify_handlers.end() != handler){
    return handler->second->notify(hWnd, message, wParam, lParam);
  }

  if (WM_COMMAND == message) {
    auto wmId = LOWORD(wParam);
    auto cmd_handler = _notify_cmd_handlers.find(wmId);
    if (_notify_cmd_handlers.end() != cmd_handler) {
      cmd_handler->second();
      return 0;
    }
  }

  return DefWindowProc(hWnd, message, wParam, lParam);
}